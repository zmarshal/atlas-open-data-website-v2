// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion
const lightCodeTheme = require('prism-react-renderer').themes.github;
const darkCodeTheme = require('prism-react-renderer').themes.dracula;

const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'ATLAS Open Data',
  tagline: 'Documentation for ATLAS Open Data.',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://atlas-outreach-data-tools.gitlab.cern.ch',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  stylesheets: [
    // Include your custom CSS file
    {
      href: '/css/custom.css',
      type: 'text/css',
    },
  ],
  plugins: [
    [
      '@docusaurus/plugin-client-redirects',
      {
        redirects: [
          // https://docusaurus.io/docs/api/plugins/@docusaurus/plugin-client-redirects
          {
            to: '/docs/category/13-tev-tutorials-for-education',
            from: '/release/2020/documentation/',
          },
        ],
      },
    ],
  ],
  
  presets: [
    [
      'classic',
      {
        docs: {
          admonitions: {
            keywords: ['faqs','notebook'],
            extendDefaults: true,
          },
          sidebarPath: require.resolve('./sidebars.js'),
	  remarkPlugins: [math],
          rehypePlugins: [katex],
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],

  stylesheets: [ {
    href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
    type: 'text/css',
    integrity:
      'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
    crossorigin: 'anonymous',
    },
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/atlas_logo.png',
      announcementBar: {
        id: 'whats_new',
        content:
          "We changed a bit the structure of the website and the location of resources; check <a rel='noopener noreferrer' href='/docs/Updates'>what's new</a>!",
        backgroundColor: '#fff3cd',
        textColor: '#091E42',
        isCloseable: true,
      },
      navbar: {
        title: 'ATLAS Open Data',
        logo: {
          alt: 'ATLAS Open Data',
          src: 'img/atlas_logo.png',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'paths',
            position: 'left',
            label: 'Get Started',
          },
          {
            type: 'dropdown',
            label: 'Tutorials',
            position: 'left',
            items: [
              {
                type: 'docSidebar',
                sidebarId: 'tutedu',
                label: 'Tutorials for education',
              },
              {
                type: 'docSidebar',
                sidebarId: 'tutresearch',
                label: 'Tutorials for research',
              },
              {
                type: 'docSidebar',
                sidebarId: 'visualization',
                label: 'Online Data Analyser',
              },
	            {
                type: 'docSidebar',
                sidebarId: 'videotuts',
                label: 'Video tutorials',
              },
            ],
          },
          {
            type: 'docSidebar',
            sidebarId: 'documentation',
            position: 'left',
            label: 'Documentation',
          },
          {
            to: '/docs/faqs',
            position: 'left',
            label: 'FAQs',
          },
          {
            to: '/docs/updates',
            position: 'left',
            label: "What's New",
          },
          {
            to: '/contact-us',
            position: 'left',
            label: 'Contact us',
          },
          {
            type: 'search',
            position: 'right',
          },
          {
            href: 'https://github.com/atlas-outreach-data-tools',
            label: 'GitHub',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Get in Touch',
            items: [
              {
                label: 'Contact Us',
                to: '/contact-us',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'The ATLAS website',
                href: 'https://atlas.cern/',
              },
              {
                label: 'The CERN Open Data portal',
                href: 'https://opendata.cern.ch/',
              },
            ],
          },
          {
            title: 'Social',
            items: [
              {
                label: 'Twitter',
                href: 'https://twitter.com/ATLASexperiment',
              },
              {
                label: 'Instagram',
                href: 'https://www.instagram.com/atlasexperiment',
              },
              {
                label: 'Facebook',
                href: 'https://www.facebook.com/ATLASexperiment/',
              },
              {
                label: 'Youtube',
                href: 'https://www.youtube.com/c/atlasexperiment',
              },
            ],
          },          
          {
            title: 'More',
            items: [
              {
                label: 'GitHub',
                href: 'https://github.com/atlas-outreach-data-tools',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.cern.ch/atlas-outreach-data-tools',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} ATLAS Collaboration. Built with Docusaurus.`,        
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
      zoom: {
      },
    }),
};

module.exports = config;

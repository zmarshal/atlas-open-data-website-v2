import "./contact-us.css";
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import React, { useRef, useState } from 'react';
import emailjs from '@emailjs/browser';
import ReCAPTCHA from 'react-google-recaptcha';
import contact from './contact-us.webp';

function ContactUs() {

  const [isCaptchaValid, setIsCaptchaValid] = useState(false);
  const [hasSubmitted, setHasSubmitted] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [submissionMessage, setSubmissionMessage] = useState('');
  const form = useRef();
  const { siteConfig } = useDocusaurusContext();

  //----------------------------------
  // Update state of the recaptcha
  //----------------------------------
  const handleCaptchaChange = (value) => {
    setIsCaptchaValid(!!value);
  };

  const sendEmail = (e) => {
    e.preventDefault();
    if (!isCaptchaValid) {
      setSubmissionMessage("Please, let us know that you are not a robot.");
      setHasSubmitted(false);
      return;
    }

    setIsSubmitting(true);
    emailjs.sendForm('service_an99pie', 'template_mwqp2zh', form.current, 'qEXGkeHRu0UJl1gpE')
      .then((result) => {
        console.log(result.text);
        setSubmissionMessage("Done! Statistics for nerds: 200 OK");
        setIsCaptchaValid(false); // Reset reCAPTCHA
        setHasSubmitted(true);
      }, (error) => {
        console.log(error.text);
        setSubmissionMessage("Mh, something went wrong. Please retry again later.");
        setHasSubmitted(false)
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  };

  return (
    <center>
    <section className="row-contact-us">
      <div className="column">
        <h1 style={{ fontSize: "4em", marginTop: '2em' }}>
          Contact us
        </h1>
        <p style={{ fontSize: '1.2rem', width: '50%' }}>Do you have questions, issues, requests, or proposals? We encourage you to check our
      <a href="../../docs/faqs"> FAQs</a> and <a href="/../../docs/Updates"> What's New</a> sections for the latest updates and common queries. 
      If you still need assistance, feel free to get in touch by filling out the form!
</p>
      <div className='contact-landing'>
          <img src={contact} alt="Contact us" />
      </div>
      </div>

      <div className="column" id="contact-form">
        <form ref={form} onSubmit={sendEmail}>
          <input type="text" name="fullName" placeholder="Your name" id="rsvpname" required />
          <input type="text" name="email" placeholder="Your email" id="rsvpemail" required />
          <input type="text" name="subject" placeholder="Subject" id="rsvpsubject" required />
          <textarea name="message" placeholder="Message" id="textrsvp" />
          <ReCAPTCHA
            sitekey="6LeXEjEpAAAAABkDqT3dHMdsQvrwjxNtBegrayjE"
            onChange={handleCaptchaChange}
          />
          <input
            id="submit"
            type="submit"
            value="SUBMIT"
            disabled={ isSubmitting}
            style={{ cursor: 'pointer', fontWeight: "bold" }}
          />
          {submissionMessage && <div className={hasSubmitted ? 'success-message' : 'error-message'}>{submissionMessage}</div>}
        </form>
      </div>
    </section>
    </center>
  );
};

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Documentation the ATLAS Open Data project.">
      <ContactUs />
      <main>
      </main>
    </Layout>
  );
}
# 🤿 Deep Dive
> *For extended use. Let's dive into what ATLAS has to offer!*

Welcome to the ATLAS open data! You are currently in the *deep dive* path, designed for students or teachers who want to use our resources over multiple sessions. 

### Setting Up Hybrid Environments
We recommend setting up one of the two hybrid environments: Docker or virtual machines. Instructions for setting them up can be found in the [Hybrid Platforms](../13TeVDoc/enviroments/hybrid) section.

### Analysis Methods
We offer two main ways to conduct analysis: through frameworks and notebooks.

- **Analysis Frameworks**: Detailed in the [Analysis Frameworks](../category/analysis-frameworks) section.
- **Analysis Notebooks**: Found in the [Analysis Notebooks](../category/analysis-notebooks) section.

The format you choose will depend on your data usage goals. If you're unsure where to start, try the [Quick Start Path](quickstart) to familiarize yourself with the analyses.

### Example Analyses
The available analyses are explained in the documentation within the [Example Analysis](../documentation/example_analyses/analysis_examples_education_2020#example-of-physics-analysis) section.

### Advanced Tutorials
If you have explored the analyses for education, consider advancing to the [PHYSLITE Tutorial](../tutresearch/physlitetut.md). Although aimed at researchers, it offers insights into how searches are conducted at ATLAS.

### General Information
For general information about the LHC and the ATLAS experiment, explore the following sections:
- [Overview of the Large Hadron Collider](../documentation/introduction/introduction_LCH)
- [Introduction to the ATLAS Experiment](../documentation/introduction/introduction_ATLAS)
- [The Standard Model of Particle Physics and Beyond](../documentation/introduction/SM_and_beyond)
- [ATLAS Data Collection](../documentation/data_collection)
- [Citing ATLAS](../documentation/ethical_legal/citation_policy)
- [The ATLAS Glossary](../documentation/appendix/glossary)

### Data for Education
If you are curious about open data for education and want to know how to access data beyond the proposed analyses, check out these sections:
- [Data for Education](../documentation/introduction/purpose_data_education)
- [8 TeV Data for Education](../documentation/overview_data/data_education_2016)
- [13 TeV Data for Education](../documentation/overview_data/data_education_2020)
- [NTuple](../documentation/data_format/ntuple)
- [Accessing the Data](../documentation/data_format/access)
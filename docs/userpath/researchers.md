# 🔬 Researchers Toolkit
> *Detailed information and resources for researchers*

Welcome to the ATLAS open data! You are currently in the researcher toolkit, which gathers information that researchers might find useful.

### Accessing the Data
To understand how to access the data and the naming convention of the files, visit the [Accessing the Data](../documentation/data_format/access_research) section. The datasets are published in the PHYSLITE format, which you can learn more about in [The PHYSLITE Data Format](../documentation/data_format/physlite) section.

### Tutorials and Hands-On Guides
To get hands-on with the data, check out the [PHYSLITE Tutorial](../tutresearch/physlitetut). This tutorial will guide you through:
- Reading PHYSLITE data
- Performing simple analysis
- Generating your own [NTuple](../documentation/data_format/ntuple)

Other turorials are found in the Tutorials tab, under [Tutorials for Research](../category/tutorials-for-research)

### General Information
For a general overview of the data for research, refer to:
- [Data for Research](../documentation/introduction/purpose_data_research)
- [2024 Release of Data for Research](../documentation/overview_data/data_research_2024)

### Citation Information
Information on how to cite the open data is available in the [Citing ATLAS](../documentation/ethical_legal/citation_policy) section.

### Additional Resources
For more general information about the collaboration, data collection processes, Monte Carlo sample generation, and more, explore the following sections:
- [Introduction to the ATLAS Experiment](../documentation/introduction/introduction_ATLAS)
- [ATLAS Data Collection](../documentation/data_collection)
- [Good Run List (GRL) Definition](../documentation/data_collection/GRL_definition)
- [Monte Carlo Production Chain](../documentation/monte_carlo/MC_production_chain)
- [Simulation Tools](../documentation/monte_carlo/simulation_tools)

### Advanced Software and Analysis Tools
For advanced software and analysis tools, refer to the [ATLAS Analysis Software Tutorial](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/). This tutorial is used by ATLAS collaboration members to learn the basics of ATLAS software and the latest physics analysis tools. 

For this you will need to setup an enviroment. We offer Docker containers that can be used to follow the tutorial. Check [Getting Started with Containers for Analysis](../tutresearch/containers)
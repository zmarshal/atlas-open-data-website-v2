# Path for Teachers
*Insert description of path for teachers*
## General information
- [Overview of the Large Hadron Collider](../documentation/introduction/introduction_LCH)
- [Introduction to the ATLAS experiment](../documentation/introduction/introduction_ATLAS)
- [The Standard Model of Particle Physics and Beyond](../documentation/introduction/SM_and_beyond)
- [ATLAS Data Collection](../documentation/data_collection)

## Information for Analyses
- [Data for Education](../documentation/introduction/purpose_data_education)
- [8 TeV Data for Education](../documentation/overview_data/data_education_2016)
- [13 TeV Data for Education](../documentation/overview_data/data_education_2020)
- [N-tuple](../documentation/data_format/ntuple)
- [Accessing the Data](../documentation/data_format/access)
- [Example Analyses with the 13 TeV Data for Education](../documentation/example_analyses/analysis_examples_education_2020)
# Virtual machine installation

**Download the latest ATLAS Open Data VM**

[![download](../13TeVDoc/pictures_vm/download_vm.png)](https://zenodo.org/record/3687320/files/ATLAS-Open-Data-ubuntu-2020-v4.ova)

This is an **Ubuntu 18.04.3 LTS** with:

* **ROOT** 6.18 (configuration all)

* **Jupyter** (bash, python2, python3, ROOT C++ kernels)

* **Extras** TensorFlow + demo git repos

* **Cite with** [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3629875.svg)](https://doi.org/10.5281/zenodo.3629875)

* The password of the VM is **root** (also to be `sudo`)
---

**Check our ~9 min video tutorial on how to install the virtual machine to don't miss any detail**

<iframe scrolling="no"  src="https://videos.cern.ch/video/OPEN-VIDEO-2020-018-002" width="100%" height="420" frameborder="0" allowfullscreen></iframe>


---

## Installation instructions

+ After you downloaded the VM file, start the VirtualBox. Go to the menu ***"File"*** **->** ***“Import Appliance...”*** (or perform the same function with the combination of buttons: **"Ctrl" + "I"**).

![path](../13TeVDoc/pictures_vm/fig-7-1.png)

+ In the window, select the downloaded ".ova" file from the **Downloads** folder and click "Next":

![path](../13TeVDoc/pictures_vm/fig-7-2.png)

+ Click "Import" without any changes:

![path](../13TeVDoc/pictures_vm/fig-7-3.png)

+ The operating system should begin importing:

![path](../13TeVDoc/pictures_vm/fig-7-4.png)

— Now your Virtual Machine with Ubuntu-Linux operating system is ready, start it by clicking ![path](../13TeVDoc/pictures_vm/fig-7-5.png) and use:

![path](../13TeVDoc/pictures_vm/fig-7-6.png)

You may come across issues with hardware virtualisation. Some computers have virtualisation disable by default.

If you do, and due to the various manufacturers, the best is to Google something like:

**"how to enable virtualisation in [insert name and model of your computer]".**


*For example*, here is a solution for Lenovo.
Enable virtualisation:
 
1.       Boot systems to BIOS with the F1 key at power on.
2.       Select the Security tab in the BIOS.
3.       Enable Intel VTT or Intel VT-d if needed.
4.       Once enabled, save the changes with F10 and allow the system to reboot.


---
# How to Run the Virtual Machine

**Take a look to this video on how to get and run some notebooks and framework in your computer**

<div style={{ textAlign: 'center' }}  >
<iframe width="100%" height="405" src="https://www.youtube.com/embed/Lj73Vjd6Nys?start=52" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

---

_ _ _
**Information:** If you want to install your operating system (in this case Ubuntu) from scratch, then see how to do it in [***this section***](appendix.md).
_ _ _


<!--


After starting your VM and running the notebooks, you may encounter the following **error** (look at the red frames in the photo below):

![path](../13TeVDoc/pictures_vm/fig-12-3.png)

To solve this **error**, follow these simple steps:

+ Сlose notebooks and terminal

+ Run the terminal again and type the following code: * `./run-server-jupyter.sh` * to start Jupiter

![path](../13TeVDoc/pictures_vm/fig-12-4.png)

+ This way you have restarted your terminal and server successfully

![path](../13TeVDoc/pictures_vm/fig-12-5.png)

+ Open the notebook in your browser and restart the *kernel* by clicking the restart button

![path](../13TeVDoc/pictures_vm/fig-12-6.png)

+ Now your terminal and server should be working and the notebooks should run without errors

![path](../13TeVDoc/pictures_vm/fig-12-7.png)
-->

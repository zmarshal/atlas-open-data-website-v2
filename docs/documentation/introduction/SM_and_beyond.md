# The Standard Model of Particle Physics and Beyond
The Standard Model (SM) of particle physics is a theoretical framework that describes the fundamental particles and their interactions, excluding gravity. It summarizes our current understanding of quantum mechanics and field theory, combining the electroweak interaction and quantum chromodynamics (QCD) into a coherent model. Despite its success in predicting experimental outcomes, the Standard Model is considered an incomplete theory, encouraging ongoing research into physics beyond the Standard Model. For a simpler description of the Standard Model, check this [Standard Model cheat sheet](https://cds.cern.ch/record/2759492/files/Standard%20Model%20-%20ATLAS%20Physics%20Cheat%20Sheet.pdf).

## Constituents of the Standard Model
### Fermions
Fermions—particles with spin-1/2—, are the building blocks of matter, categorized into quarks and leptons. 

- **Quarks** possess fractional electric charges and participate in all four fundamental forces, notably the strong interaction, mediated by gluons. They are confined within hadrons due to color confinement and are found in six flavors: up (u), down (d), charm (c), strange (s), top (t), and bottom (b), each with corresponding anti-particles.
- **Leptons**, carrying integer electric charges, interact through the electromagnetic and weak forces but not the strong force. The lepton family includes electrons (e), muons (μ), tau (τ), and their respective neutrinos (νₑ, ν<sub>μ</sub>, ν<sub>τ</sub>), which are electrically neutral and interact primarily via the weak force.

### Bosons
Bosons—particles with integer spin-, mediate the fundamental forces. The photon (γ) is the quantum of electromagnetic force, massless and chargeless. The W and Z bosons mediate the weak force, responsible for processes like beta decay, with the Z boson being neutral and the W boson existing in positively and negatively charged forms. The gluons (g) are the force carriers for the strong force, binding quarks within hadrons, and are unique in carrying the color charge. The Higgs boson (H), a scalar particle with spin-0, arises from the Higgs field, imparting mass to other particles through the Higgs mechanism.

import standardmodel from '../images/standard_model.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={standardmodel} alt="Description of the image" style={{width: 650, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: Particles of the Standard Model.</figcaption>
</figure>

## Fundamental Interactions
The SM includes three of the four known [fundamental forces](https://en.wikipedia.org/wiki/Fundamental_interaction): the electromagnetic, weak, and strong nuclear forces. It is framed within the context of [gauge theories](https://en.wikipedia.org/wiki/Gauge_theory), where each force is described by specific gauge symmetries: U(1) for electromagnetism, SU(2) for the weak force, and SU(3) for the strong force. The electroweak theory unifies the electromagnetic and weak interactions under a combined gauge group SU(2)×U(1), predicting the existence of the W, Z, and Higgs bosons, all of which have been experimentally confirmed.

## Introduction to the Higgs Boson
The Standard Model (SM) of particle physics postulates the existence of a complex scalar doublet with a [vacuum expectation value](https://en.wikipedia.org/wiki/Vacuum_expectation_value), which spontaneously breaks the electroweak symmetry, gives masses to all the massive elementary particles in the theory, and gives rise to a physical scalar known as the **Higgs boson**. The [Higgs boson](https://home.cern/topics/higgs-boson) is a fundamental particle, first observed by ATLAS and CMS in 2012, although theorised in the 1960s by Brout, Englert, and Higgs. The Higgs boson is the carrier particle for the Higgs field, a field present throughout our universe, which gives particles their mass. The more a particle interacts with the Higgs field, the higher its mass.

An illustration of the "Mexican hat" shape of the Higgs field potential is presented below:


import potential from './../../physics/pictures/potential.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={potential} alt="Mexican hat shape of the Higgs field potential" style={{width: 700, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig.2: Higgs potential.</figcaption>
</figure>

### Higgs boson production

Standard Model production of the Higgs boson at the LHC is dominated by the gluon fusion process (ggF), followed by the vector-boson fusion process (VBF). Associated production also have sizeable contributions, with a W or Z boson (VH) or a pair of top quarks (qqH).

The representative Feynman diagrams for the production processes are shown below:

import diagrams from './../../physics/pictures/diagrams.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={diagrams} alt="Feynman diagrams for Higgs production" style={{width: 400, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig.3: Feynman diagrams for Higgs production.</figcaption>
</figure>

The figure below shows the Standard Model Higgs boson production cross sections as a function of the centre-of-mass energy. If the cross-section value (left axis) is multiplied by the luminosity of the dataset to be analysed, that is effectively how many Higgs bosons are expected to be produced (for different LHC energies).

import higgsxsec from './../../physics/pictures/7-14.xsec.jpg';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={higgsxsec} alt="Higgs boson production cross sections as a function of the centre-of-mass energy" style={{width: 700, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig.4: Higgs boson production cross sections as a function of the centre-of-mass energy.</figcaption>
</figure>

Quantum chromodynamics \(QCD\) and Electroweak \(EW\) models are used to predict the production cross sections. Next-to-leading order\(NLO\) and next-to-next-to leading order \(NNLO\) calculations are carried. High order corrections are required to achieve the desired precision for these predictions.

Currently, the SM Higgs boson mass has been measured to be 125.09 ± 0.24 [GeV](https://en.wikipedia.org/wiki/Electronvolt) by combining ATLAS and CMS [measurements](https://arxiv.org/abs/1503.07589).

### Higgs boson decay

According to the Standard Model \(SM\), the Higgs boson can decay into pairs of fermions or bosons. The Higgs boson mass is not predicted by the SM, but once measured the production cross sections and branching ratios can be precisely calculated.

The Standard Model Higgs boson decay branching ratios and total width are shown in the figure below \[[PDG](http://pdg.lbl.gov/2013/reviews/rpp2013-rev-higgs-boson.pdf)\]. You can see that the decay modes change depending on the mass of the Higgs boson. The figure represents how likely the Higgs boson is going to decay into a certain particle, or group of particles, depending on its mass.

import higgsbrlm from './../../physics/pictures/Higgs_BR_LM.jpg';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
<img src={higgsbrlm} alt="Decay modes change depending on the mass of the Higgs boson" style={{width: 500, marginBottom: '0px'}} />
<figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig.5: Decay modes in function of the Higgs mass.</figcaption>
</figure>

The following table displays the branching ratios and the relative uncertainty for a Standard Model Higgs boson with a mass of 125 GeV.

import brtabledpg from './../../physics/pictures/BRtablePDG.jpg';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
<img src={brtabledpg} alt="Branching ratios and the relative uncertainty for a Standard Model Higgs boson with a mass of 125 GeV" style={{width: 500, marginBottom: '0px'}} />
<figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig.6: Branching ratios and the relative uncertainty for a Standard Model Higgs boson with a mass of 125 GeV.</figcaption>
</figure>

The decay mode with the highest branching ratio \(BR\) is the decay to [hadrons](https://en.wikipedia.org/wiki/Hadron), with around 70%, which is not easy to detect due to [multijet QCD](https://cds.cern.ch/record/1951336) backgrounds. A large fraction of the [leptonic](https://en.wikipedia.org/wiki/Lepton) decays are to a pair of [neutrinos](https://en.wikipedia.org/wiki/Neutrino), with around 20%, which are difficult to detect since the neutrinos hardly interact with matter. The decay to pairs of electrons, muons and tau-leptons have a BR of about 10% of the total. In fact, the tau [life time](https://en.wikipedia.org/wiki/Particle_decay) is very short, 3x10<sup>-13</sup>s, so it can be reconstructed only from its decay products. The efficiency of reconstructing tau-leptons is much lower than that of electrons and muons.

## Beyond the Standard Model
The Standard Model is a very successful theoretical framework. It has allowed verification and prediction of multiple phenomenom that are observed with experimentally. However, it does not account for every observed phenomenom, which derives in a need to look Beyond the Standard Model (BSM). Theories BSM attemp to answer question for which we don't have an answer within the Standard Model. The verification or refutation of these theories is one of the objectives of the LHC program.

### Evidence for Physics Beyond the Standard Model
The search beyond the Standard Model arises from experimental observations that cannot be explained by the SM, unobserved predictions, or values and parameters for which the theory has no explanation. Some of these are briefly presented below:

- **Matter-antimatter asymetry**: According to the SM, in the early universe, the same amount of matter and antimater should have been created. However, the universe as we see it is made of entirely of matter. This is known as matter-antimatter asymetry.
- **Neutrino mass**: It is not known through which mecanism neutrinos adquire mass or why their masses are small in comparison with every other fermion. 
- **Dark matter and dark energy**: The existence of dark matter is inferred for the gravitational effects it have on visible matter, and dark energy is used to explain the expansion of the universe. However, these are not yet included in the SM.
- **Quantum description of gravity**: The gravitational force is not included in the Standard Model and it has not been possible to construct a quantum field theory that reconciles gravity with particle physics because general relativity is not [renormalizable](https://en.wikipedia.org/wiki/Renormalization).
- **Origin of masses**: The standard model does not explain the origin of the [Brout–Englert–Higgs mechanism](https://home.cern/science/physics/origins-brout-englert-higgs-mechanism), it is only known that it is necessary to coincide with experimental observations. 
- **Origin of mixes**: The mass of the fermions and the mixing angles of the quarks seem to present a pattern. This suggests the existence of some underlying mechanism that generates the hierarchies. At the moment, they only represent parameters in the model and it is not understood why they present a structure.
- **Strong CP problem**: Charge-parity (CP) violation is predicted in QCD, but this has not been observed experimentally.
- **Hierarchy problem**: The SM has two seemingly independent mass scales: the electroweak and the Planck scale. The ratio of these is small, 10<sup>−17</sup>, and raises the question about underlying dynamics that links the two mass scales.

### Search for New Physics
In physics Beyond the Standard Model (BSM), multiple searches are conducted to explore theory and phenomena that is beyond our current understanding. These searches look into the potential existence and characteristics of new particles.

#### $Z'$-boson
One BSM search targets the hypothetical $Z'$-boson, presumed to decay into pairs of leptons or quarks. This search uses strategies similar to those of the Higgs boson exploration but focuses on detecting a heavy boson with similar couplings to the familiar Standard Model $Z$-boson. The search includes various decay modes, including electron, muon, tau lepton, and quark pairs. Each mode provides unique insights into detector behaviors at high energies and the challenges of identifying specific decay paths, such as those involving tau leptons or top quark pairs.

The pursuit of a hypothetical $Z'$-boson addresses several questions. This includes the pursue for unification of forces, the nature of dark matter, the origin of neutrino masses, and anomalies that the Standard Model fails to explain. The $Z'$-boson represents a potential mediator of new forces, could help understanding the distribution of dark matter, and might resolve discrepancies in experimental data, such as the anomalous magnetic moment of the muon.

#### Dark matter
Many interesting extensions to the Standard Model include some form of dark matter candidate, focusing on the signature of a "mono-jet" event where a jet recoils against missing transverse energy (MET), suggesting the presence of undetected particles. This search looks on how dark matter might manifest in a detector and the how momentum imbalance can be used as a tool for discovery.

#### $W'$-boson
The search for the $W'$-boson is conceptually similar to the search for a  $Z'$-boson. This search also includes various decay channels including electron-neutrino, muon-neutrino, tau-neutrino pairs, and quark pairs. Each channel gives information on different aspects of the detector's performance under various conditions and the nature of decay signatures, especially those involving neutrinos which imply missing energy scenarios.

Like the Z' boson, a W' could reveal alternative mechanisms of symmetry breaking, mass generation, and possibly mediate new interactions with dark matter. 

#### Di-Higgs
The discovery of a Higgs boson with a mass of about 125 GeV in initiated a major effort to understand the possible connections of this particle with new physics phenomena. In particular, the possibility that heavy particles beyond the Standard Model (BSM) may couple to the Higgs boson has attracted much attention. Several BSM models including extra dimensions [(Phys. Rev. D 92 (2015) 116005)](https://arxiv.org/abs/1512.01766) or extensions of the Higgs sector, including models with additional [weak isospin](https://en.wikipedia.org/wiki/Weak_isospin) singlets, doublets or triplets [(Phys. Rept. 516 (2012) 1, Phys. Rev. D 90 (2014) 015007, JHEP 09 (2022) 011)](https://arxiv.org/pdf/2311.15956); predict the existence of new particles decaying into Higgs boson pairs.

#### Supersymmetry
Supersymmetry (SUSY) includes a wide array of potential new particles and signatures. It is not a single model, rather, there is a wide variety of supersymmetric extensions of the Standard Model. These involve different superpartner spectra, and therefore different experimental signatures. From stop squarks decaying into the lightest neutralino to electroweakino production leading to multi-lepton final states. Each model underlines different aspects of potential BSM physics, from compressed mass spectra to the inclusion of gravitino in decay processes.
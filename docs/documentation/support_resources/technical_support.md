# Technical support
> 🚧 Feb 2024 - Under construction

The datasets released by the ATLAS collaboration represent a snapshot of an ongoing, evolving scientific labor. As with any project of this magnitude, involving numerous contributors and complex systems, it is natural for the data and accompanying materials to undergo continuous refinement and updates. We strive for accuracy and completeness, but users should be aware that, as with all scientific research, the data and its interpretation may evolve over time.

We acknowledge that the datasets and resources provided may not be perfect and could contain errors or inconsistencies that are intrinsic to the nature of research data. ATLAS is committed to continually improving and updating these resources. We welcome constructive feedback and contributions from the user community, which are invaluable for enhancing the quality and usability of the data.

For inquiries, suggestions, or to report issues with the data, users are encouraged to contact the ATLAS collaboration through the following channel: ATLAS Data Support. Our team is dedicated to providing support and guidance to researchers using these resources, facilitating a collaborative and dynamic scientific environment.


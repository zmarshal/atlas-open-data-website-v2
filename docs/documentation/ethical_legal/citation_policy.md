# Citing ATLAS
The public datasets are accessible on the [CERN Open Data portal](https://opendata.cern.ch), under [Creative Commons CC0 license](https://creativecommons.org/publicdomain/zero/1.0/). 

Any paper published using these data should cite the corresponding DOI of the datasets. The citation should be similar to this:

> ATLAS Collaboration (2020). *ATLAS simulated samples collection for jet reconstruction training, as part of the 2020 Open Data release.* CERN Open Data Portal. DOI:10.7483/OPENDATA.ATLAS.L806.5CKU

A few additional useful papers for citation are provided below. Please ensure that the ATLAS Collaboration is acknowledged as well. Our preferred acknowledgement is:

> “We acknowledge the work of the ATLAS Collaboration to record or simulate, reconstruct, and distribute the Open Data used in this paper, and to develop and support the software with which it was analysed."

For citing the ATLAS Detector:
> ATLAS Collaboration. *"The ATLAS Experiment at the CERN Large Hadron Collider: A Description of the Detector Configuration for Run 3.”* arXiv:2305.16623 (2023).

## ⚠️ Disclaimer
Neither ATLAS nor CERN endorse any works, scientific or otherwise, produced using these data.
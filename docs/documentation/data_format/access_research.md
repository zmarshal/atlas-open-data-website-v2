# Accessing the Data for Research

The ATLAS Collaboration, as part of its commitment to open science, has made an extensive array of data available through the [CERN Open Data portal](https://opendata.cern.ch). This includes specialized dataset for research. All datasets, whether for educational outreach or advanced research, are hosted on the [CERN Open Data portal](https://opendata.cern.ch), ensuring that anyone with internet access can explore it. 

## How the Data is Organized
The research data is categorized into two main groups: detector data and Monte Carlo simulations.

### Detector Data
This category includes two comprehensive datasets from the data collected in 2015 and 2016 during the Run 2 period.

Within each dataset, you can find downloadable data organized in different containers for specific runs, according to the [Good Run List](../data_collection/GRL_definition), under "file indexes".

### Monte Carlo Simulations
This category includes nine distinct datasets:
- Top nominal and systematics
- Jet nominal and systematics
- Higgs nominal and systematics
- Electroweak boson nominal
- SUSY signals
- Exotic signals

Within each dataset, you can find downloadable data organized in different containers for specific processes under "file indexes".

## Naming convention
In ATLAS, we use specific nomenclature for naming files to ensure they are easily identifiable. The naming conventions vary based on the type of file (Monte Carlo simulations or experimental data) to maintain clarity and organization.

### Monte Carlo Simulations
The names for Monte Carlo simulations are composed by different substrings, separated by a dot:
```
campaing.dataset_id.short_description.production_step.data_format.processing_tags
```
Each part represents the following:
1. campaing: Indicates the MC simulation campaing and center of mass energy, when relevant. For example, for the released data from the MC20 campaing of proton-proton collisions at 13TeV of center of mass energy is "mc20_13TeV".
2. dataset_id: An 6 to 8 character numerical identifier, different for each dataset.
3. short_description: Indicates the simulation tools used and the physical process described by the dataset. Common simulation tools are Powheg, Pythia, Sherpa, among others. You can check the list of [simulation tools](../monte_carlo/simulation_tools) or [common abbreviations](https://home.cern/) for more information about you can find on this substring.
4. production_step: The production step that generated the dataset. For the release data it is always "deriv" from derivation.
5. data_format: The dataset format. All the released data is in PHYSLITE format, so this substring is always "DAOD_PHYSLITE".
6. processing_tags: These tags indicate the configuration of the software used in each production step in the creation of the dataset. In the released MC data we have four tags: e-tag, for the event generation configuration; s-tag, for the simulation configuration; r-tag, for the reconstruction configuration; and a p-tag for physlite production. To understan more about the tags you can read about the [MC production chain](../monte_carlo/MC_production_chain)

An example of a name would be:
```
mc20_13TeV.364350.Sherpa_224_NNPDF30NNLO_Diphoton_myy_0_50.deriv.DAOD_PHYSLITE.e7081_s3681_r13167_p5855
```
Which is: a monte carlo simulation from the mc20 project, at 13 TeV of center of mass energy. It contains the simulation of diphoton events (events where two photons are produced) using Sherpa 2.24 as event generator, particularly using the [NNPDF](https://nnpdf.mi.infn.it) 3.0 at next-to-next-to-leading order precision. This dataset focuses on events where the invariant mass of the photon pair lies between 0 and 50 GeV. The dataset is in PHYSLITE format, and we can identify it by its ID "364350".

### Detector Data
The naming convention for detector data is similar in form, but differs in content:
```
project.period.data_stream.production_step.data_format.processing_tags
```
1. project: Indicates the data taking year and the center of mass energy.
2. dataset_id: An 8 character numerical identifier, different for each dataset.
3. data_stream: The released datasets include two primary data streams. The first is "physics_Main," which refers to the main physics data stream used for general-purpose physics analyses. The second is "physics_HardProbes", which pertains to data derived from lead nucleus collisions.
4. production_step: The production step that generated the dataset. For the release data it is always "deriv" from derivation.
5. data_format: The dataset format. All the released data is in PHYSLITE format, so this substring is always "DAOD_PHYSLITE".
6. processing_tags: For detector data this inncludes an r-tag and p-tag.

An example of a detector dataset name:
```
data16_13TeV.00298633.physics_Main.deriv.DAOD_PHYSLITE.r13286_p4910_p5631
```
This is a detector dataset from the data taking period of 2016. It is a dataset for general-purpose physics analyses. Is is in PHYSLITE format, and we can identify it by its ID "00298633".
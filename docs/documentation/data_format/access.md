# Accessing the Data for Education

The ATLAS Collaboration, as part of its commitment to open science, has made an extensive array of data available through the [CERN Open Data portal](https://opendata.cern.ch). This includes datasets from 8 TeV and 13 TeV proton-proton collisions for educational purposes. These resources offer an opportunity for users to engage with real data from the Large Hadron Collider (LHC).

All datasets, whether for educational outreach or advanced research, are hosted on the [CERN Open Data portal](https://opendata.cern.ch), ensuring that anyone with internet access can explore the fundamental workings of the universe. The data spans various operational periods and energy levels.

## 13 TeV Data for Education
The 13 TeV data, specifically curated for educational purposes, is provided in ROOT format. This format is favored for its compatibility with the ROOT analysis toolkit, widely adopted in the physics community for data processing, analysis, and visualization.

Users can select and download data based on specific event criteria such as the presence of leptons, jets, or photons, and whether the data is derived from recorded collisions or Monte Carlo simulations. The available datasets are organized into collections to facilitate targeted educational activities and research inquiries.

### Datasets in ROOT format

Below is a table outlining the available collections for the 13 TeV data, complete with descriptions and direct download links to their corresponding ZIP files. For a RAW view of the repository of the files 13 TeV dataset per collection, click [here](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/).

| Description | Name  | link to ZIP file |
|-------------|---------|------------------|
| Events selected with at least one lepton (electron or muon) and exactly one large-Radius jet (R = 1.0) | 1largeRjet1lep | [5.5 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/1largeRjet1lep.zip)        |
| Events selected with exactly one lepton (electron or muon). This is a very large collection, so, it was divided into three ZIP files| 1lep | [17 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/Data-1lep.zip), [20 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/MC-1-1lep.zip), [21 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/MC-2-1lep.zip) |
| Events selected with exactly one lepton (electron or muon) and exactly one hadronic-reconstructed tau | 1lep1tau | [1.3 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/1lep1tau.zip)   |
| Events selected with at least two leptons (electron or muon) | 2lep | [24 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/2lep.zip)   |
| Events selected with exactly three leptons (electron or muon) | 3lep | [1.0 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/3lep.zip)   |
| Events selected with at least four leptons (electron or muon) | 4lep | [427 Mb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/4lep.zip)   |
| Events selected with at least two photons | GamGam | [1.5 Gb](https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/GamGam.zip)   |
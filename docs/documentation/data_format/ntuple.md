# NTuple
An NTuple, in the context of particle physics, is a simplified data structure that allows for straightforward access to the parameters of interest in an event. These files store data as rows of values, similar to an spreadsheet, where each row represents a collision event and each column a variable associated with that event.

## Structure and Accessibility
The structure of NTuple files is made of a series of branches, each corresponding to a specific variable from the ATLAS detector. These branches may include information on particle types, momenta, energies, and other variables that help to the understanding the particle interactions observed in the detector. NTuple files are particularly valued in educational settings due to their ease of use. They require minimal preprocessing and can be easily accessed using standard data analysis software commonly employed in educational environments, such as ROOT.

## Benefits for Education
- **Simplicity**: The simple, tabular format of NTuples makes them accessible for students and educators without the need for extensive background in data analysis or the complexities of particle physics detectors.
- **Flexibility**: Educators can easily extract specific datasets from NTuple files to tailor lessons to various educational objectives, from simple demonstrations of particle tracks to more complex analyses like identifying particle decays.
- **Interactivity**: NTuple files can be used in interactive environments that facilitate hands-on learning, such as Jupyter notebooks, where students can visualize and manipulate data in real-time.

## Using NTuple Files
To work with NTuple files from the ATLAS Open Data
- **Access**: Users can download NTuple files directly from the CERN Open Data portal.
- **Software**: Open-source tools like ROOT can be employed to analyze the data. Tutorials and examples are provided to help users get started.
- **Analysis**: Users can perform a variety of analyses, from plotting simple distributions to conducting multi-variable analyses, using the structured data provided.

## Details About the Published Data

### 8 TeV Data for Education
The resulting format of the datasets is a TTree tuple (or ROOT ntuple) with 45 branches as detailed in the table below:

<details>
<summary>Open to view full list</summary>

| Tuple branch name | Type | Description |
|-------------------|------|-------------|
| `runNumber` | `int` | number uniquely identifying ATLAS data-taking run |
| `eventNumber` | `int` | event number and run number combined uniquely identifies event |
| `channelNumber` | `int` | number representing leptonic channel |
| `mcWeight` | `float` | weight of a simulated event |
| `pvxp_n` | `int` | number of primary vertices |
| `vxp_z` | `float` | z-position of the primary vertex |
| `trigE` | `bool` | signifies whether event passes a standard electron trigger |
| `trigM` | `bool` | signifies whether event passes a standard muon trigger |
| `passGRL` | `bool` | signifies whether event passes a data quality assessment |
| `hasGoodVertex` | `bool` | signifies whether the event has at least one good vertex |
| `lep_n` | `int` | number of preselected leptons |
| `lep_truthMatched` | `vector<bool>` | indicates whether the lepton is matched to a truth lepton |
| `lep_trigMatched` | `vector<bool>` | indicates whether the lepton is the one triggering the event |
| `lep_pt` | `vector<float>` | transverse momentum of the lepton |
| `lep_eta` | `vector<float>` | pseudorapidity, η, of the lepton |
| `lep_phi` | `vector<float>` | azimuthal angle, φ, of the lepton |
| `lep_E` | `vector<float>` | energy of the lepton |
| `lep_z0` | `vector<float>` | z-coordinate of the track associated to the lepton wrt. the primary vertex |
| `lep_charge` | `vector<float>` | charge of the lepton |
| `lep_flag` | `vector<int>` | used to implement object cuts |
| `lep_type` | `vector<int>` | number signifying the lepton type (e, mu, tau) of the lepton |
| `lep_ptcone30` | `vector<float>` | used to ensure tracking isolation of the lepton |
| `lep_etcone20` | `vector<float>` | used to ensure calorimeter isolation of the lepton |
| `lep_trackd0pvunbiased` | `vector<float>` | d0 of the track associated to the lepton at the point of closest approach (p.c.a.) |
| `lep_tracksigd0pvunbiased` | `vector<float>` | d0 significance of the track associated to the lepton at the p.c.a. |
| `met_et` | `float` | Transverse energy of the missing momentum vector |
| `met_phi` | `float` | Azimuthal angle of the missing momentum vector |
| `jet_n` | `int` | number of selected jets |
| `jet_pt` | `vector<float>` | transverse momentum of the jet |
| `jet_eta` | `vector<float>` | pseudorapidity, η, of the jet |
| `jet_phi` | `vector<float>` | azimuthal angle, φ, of the jet |
| `jet_E` | `vector<float>` | energy of the jet |
| `jet_m` | `vector<float>` | invariant mass of the jet |
| `jet_jvf` | `vector<float>` | fraction of the total momentum of tracks in the jet |
| `jet_trueflav` | `vector<int>` | true flavor of the jet |
| `jet_truthMatched` | `vector<int>` | information whether the jet matches a jet on truth level |
| `jet_SV0` | `vector<float>` | weight from algorithm that reconstructs secondary vertices associated with a jet |
| `jet_MV1` | `vector<float>` | weight from algorithm based on multi variate technique |
| `scaleFactor_BTAG` | `float` | scalefactor for btagging |
| `scaleFactor_ELE` | `float` | scalefactor for electron efficiency |
| `scaleFactor_JVFSF` | `float` | scalefactor for jet vertex fraction |
| `scaleFactor_MUON` | `float` | scalefactor for muon efficiency |
| `scaleFactor_PILEUP` | `float` | scalefactor for pileup reweighting |
| `scaleFactor_TRIGGER` | `float` | scalefactor for trigger |
| `scaleFactor_ZVERTEX` | `float` | scalefactor for z-vertex reweighting |

</details>

### 13 TeV Data for Education
The full list of ROOT branches and variables contained within the this dataset is presented in the list below:


<details>
<summary>Open to view full list</summary>

| Tuple branch name            | C++ type         | Variable description                                                                   |
|------------------------------|:----------------:|-----------------------------------------------------------------------------------------|
| `runNumber`                  | `int`            | number uniquely identifying ATLAS data-taking run   |
| `eventNumber`                | `int`            | event number and run number combined uniquely identifies event | 
| `channelNumber`              | `int`            | number uniquely identifying ATLAS simulated dataset |
| `mcWeight`                   | `float`          | weight of a simulated event |
| `XSection`                   | `float`          | total cross-section, including filter efficiency and higher-order correction factor    |
| `SumWeights`                 | `float`          | generated sum of weights for MC process |
| `scaleFactor_PILEUP`         | `float`          | scale-factor for pileup reweighting |
| `scaleFactor_ELE`            | `float`          | scale-factor for electron efficiency |
| `scaleFactor_MUON`           | `float`          | scale-factor for muon efficiency |
| `scaleFactor_PHOTON`         | `float`          | scale-factor for photon efficiency |
| `scaleFactor_TAU`            | `float`          | scale-factor for tau efficiency  |
| `scaleFactor_BTAG`           | `float`          | scale-factor for b-tagging algorithm at 70% efficiency |
| `scaleFactor_LepTRIGGER`     | `float`          | scale-factor for lepton triggers |
| `scaleFactor_PhotonTRIGGER`  | `float`          | scale-factor for photon triggers |
| `trigE`                      | `bool`           | boolean whether event passes a single-electron trigger |
| `trigM`                      | `bool`           | boolean whether event passes a single-muon trigger |
| `trigP`                      | `bool`           | boolean whether event passes a diphoton trigger |
| `lep_n`                      | `int`            | number of pre-selected leptons |
| `lep_truthMatched`           | `vector<bool>`   | boolean indicating whether the lepton is matched to a simulated lepton |
| `lep_trigMatched`            | `vector<bool>`   | boolean indicating whether the lepton is the one triggering the event |
| `lep_pt`                     | `vector<float>`  | transverse momentum of the lepton |
| `lep_eta`                    | `vector<float>`  | pseudo-rapidity, $\eta$, of the lepton |
| `lep_phi`                    | `vector<float>`  | azimuthal angle, $\phi$, of the lepton |
| `lep_E`                      | `vector<float>`  | energy of the lepton |
| `lep_z0`                     | `vector<float>`  | z-coordinate of the track associated to the lepton wrt. primary vertex |
| `lep_charge`                 | `vector<int>`    | charge of the lepton |
| `lep_type`                   | `vector<int>`    | number signifying the lepton type (e or µ) |
| `lep_isTightID`              | `vector<bool>`   | boolean indicating whether lepton satisfies tight ID reconstruction criteria |
| `lep_ptcone30`               | `vector<float>`  | scalar sum of track pT in a cone of R=0.3 around lepton, used for tracking isolation |
| `lep_etcone20`               | `vector<float>`  | scalar sum of track ET in a cone of R=0.2 around lepton, used for calorimeter isolation|
| `lep_trackd0pvunbiased`      | `vector<float>`  | d0 of track associated to lepton at point of closest approach (p.c.a.) |
| `lep_tracksigd0pvunbiased`   | `vector<float>`  | d0 significance of the track associated to lepton at the p.c.a.        |
| `met_et`                     | `float`          | transverse energy of the missing momentum vector|
| `met_phi`                    | `float`          | azimuthal angle of the missing momentum vector  |
| `jet_n`                      | `int`            | number of pre-selected jets |
| `jet_pt`                     | `vector<float>`  | transverse momentum of the jet |
| `jet_eta`                    | `vector<float>`  | pseudo-rapidity, $\eta$, of the jet |
| `jet_phi`                    | `vector<float>`  | azimuthal angle, $\phi$, of the jet |
| `jet_E`                      | `vector<float>`  | energy of the jet |
| `jet_jvt`                    | `vector<float>`  | jet vertex tagger discriminant of the jet |
| `jet_trueflav`               | `vector<float>`  | flavor of the simulated jet |
| `jet_truthMatched`           | `vector<float>`  | output of the multivariate b-tagging algorithm of the jet |
| `photon_n`                   | `int`            | number of pre-selected photons |
| `photon_truthMatched`        | `vector<bool>`   | boolean indicating whether the photon is matched to a simulated photon |
| `photon_trigMatched`         | `vector<bool>`   | boolean indicating whether the photon is the one triggering the event |
| `photon_pt`                  | `vector<float>`  | transverse momentum of the photon |
| `photon_eta`                 | `vector<float>`  | pseudo-rapidity of the photon |
| `photon_phi`                 | `vector<float>`  | azimuthal angle of the photon |
| `photon_E`                   | `vector<float>`  | energy of the photon|
| `photon_isTightID`           | `vector<bool>`   | boolean indicating whether photon satisfies tight identification reconstruction criteria|
| `photon_ptcone30`            | `vector<float>`  | scalar sum of track pT in a cone of R=0.3 around photon |
| `photon_etcone20`            | `vector<float>`  | scalar sum of track ET in a cone of R=0.2 around photon |
| `photon_convType`            | `vector<int>`    | information whether and where the photon was converted |
| `largeRjet_n`                | `int`            | number of pre-selected large-R jets |
| `largeRjet_pt`               | `vector<float>`  | transverse momentum of the large-R jet |
| `largeRjet_eta`              | `vector<float>`  | pseudo-rapidity of the large-R jet |
| `largeRjet_phi`              | `vector<float>`  | azimuthal angle of the large-R jet |
| `largeRjet_E`                | `vector<float>`  | energy of the large-R jet |
| `largeRjet_m`                | `vector<float>`  | invariant mass of the large-R jet |
| `largeRjet_truthMatched`     | `vector<int>`    | information whether the large-R jet is matched to a simulated large-R jet |
| `largeRjet_D2`               | `vector<float>`  | weight from algorithm for W/Z-boson tagging |
| `largeRjet_tau32`            | `vector<float>`  | weight from algorithm for top-quark tagging |
| `tau_n`                      | `int`            | number of pre-selected hadronically decaying $\tau$-lepton |
| `tau_pt`                     | `vector<float>`  | transverse momentum of the hadronically decaying $\tau$-lepton |
| `tau_eta`                    | `vector<float>`  | pseudo-rapidity of the hadronically decaying $\tau$-lepton |
| `tau_phi`                    | `vector<float>`  | azimuthal angle of the hadronically decaying $\tau$-lepton |
| `tau_E`                      | `vector<float>`  | energy of the hadronically decaying $\tau$-lepton |
| `tau_charge`                 | `vector<int>`    | charge of the hadronically decaying $\tau$-lepton |
| `tau_isTightID`              | `vector<bool>`   | boolean indicating whether hadronically decaying $\tau$-lepton satisfies tight ID reconstruction criteria |
| `tau_truthMatched`           | `vector<bool>`   | boolean indicating whether the hadronically decaying $\tau$-lepton is matched to a simulated $\tau$-lepton |
| `tau_trigMatched`            | `vector<bool>`   | boolean signifying whether the $\tau$-lepton is the one triggering the event|
| `tau_nTracks`                | `vector<int>`    | number of tracks in the hadronically decaying $\tau$-lepton decay |
| `tau_BDTid`                  | `vector<float>`  | output of the multivariate algorithm discriminating hadronically decaying $\tau$-leptons from jets |
| `ditau_m`                    | `float`          | di-$\tau$ invariant mass using the missing-mass calculator |
| `lep_pt_syst`                | `vector<float>`  | single component syst. uncert. (lepton momentum scale and resolution) affecting `lep_pt` |
| `met_et_syst`                | `float`          | single component syst. uncert. ($E_T^{miss}$ scale and resolution) affecting `met_pt` |
| `jet_pt_syst`                | `vector<float>`  | single component syst. uncert. (jet energy scale) affecting jet_pt |
| `photon_pt_syst`             | `vector<float>`  | single component syst. uncert. (photon energy scale and resolution) affecting `photon_pt`|
| `largeRjet_pt_syst`          | `vector<float>`  | single component syst. uncert. (large-R jet energy resolution) affecting `largeRjet_pt`|
| `tau_pt_syst`                | `vector<float>`  | single component syst. uncert. ($\tau$ lepton reconstruction and energy scale) affecting `tau_pt`|

</details>

## Evolution from the 8 TeV release (2016) to the 13 TeV release (2020)

The evolution of the ATLAS Open Data and the tuple structure from the [8 TeV release](http://opendata.cern.ch/search?page=1&size=20&experiment=ATLAS&collision_energy=8TeV&collision_type=pp) in 2016 to the [13 TeV release].(./files.md) in 2020 are depicted below: 

![](../../13TeVDoc/pictures/fig_13a.png)

![](../../13TeVDoc/pictures/fig_13c.png)
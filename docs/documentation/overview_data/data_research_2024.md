import DownloadLink from '@site/src/components/DownloadLink';
import React from 'react';
import FilterableTable from '@site/src/components/FilterableTable';

# 13 TeV Data for Research

This is ATLAS collaboration's first release of open data for research. In compliance with the established [Open Data Policy](https://opendata.cern.ch/docs/cern-open-data-policy-for-lhc-experiments), ATLAS is releasing 25% of the 2015 and 2016 proton–proton main physics stream datasets. However, this effort is part of a broader commitment to open science, facilitating new paths of exploration across the scientific community.

## Overview of the Data
The selection of data for release includes the 2015 and 2016 runs, offering real collision data and Monte Carlo (MC) simulations that support a wide array of general analyses. The datasets span Standard Model processes, systematic uncertainty evaluations, and Beyond the Standard Model (BSM) signal processes, based on benchmark models from recent publications. 

The measurements from proton-proton collisions and lead nucleus collisions were selected based on the [Good Run Lists](../data_collection/GRL_definition) (GRLs), ensuring high quality and reliability for scientific exploration.

The MC samples, part of the MC20a simulation campaign, are tailored to represent double the luminosity of the corresponding data to ensure comprehensive coverage for analysis. It includes nine distinc groups:
- Top nominal and systematics
- Jet nominal and systematics
- Higgs nominal and systematics
- Electroweak boson nominal
- SUSY signals
- Exotic signals

For Heavy Ion research, the 2015 lead-lead data, constituting approximately 25.3% of the total Run 2 lead-lead data, will be released in a similar format. This format will include specific heavy ion event information, such as track four-vectors and centrality.

To understand how to access the data, check the section for [Accessing the Data for Research](../data_format/access_research)

## Metadata
Below, you will find the metadata for all the samples, which includes comprehensive information such as:

- **Dataset ID**: This is a unique identifier assigned to each dataset. It ensures that each dataset can be uniquely referenced and accessed.
- **Physics Short**: This is an abbreviated name that provides key information about the dataset. To know more about how to read them, check the subsection about [naming convention](../data_format/access_research#naming-convention).
- **Cross-Section (in pb)**: Represents the probability of a particular interaction occurring, measured in picobarns (pb). It is a fundamental parameter that helps understanding the likelihood of specific particle interactions under given conditions.
- **Filter Efficiency**: Measure of the effectiveness of the selection criteria applied to the data. It indicates the fraction of events that pass the filters applied during the data processing stages.
- **K-factor**: Multiplicative correction factor used to account for higher-order effects in theoretical calculations. It adjusts the leading-order theoretical predictions to better match the observed data by incorporating next-to-leading order (NLO) or next-to-next-to-leading order (NNLO) corrections.
- **Process Description**: Provides a brief description of the physics process being studied. For instance, "H->$\gamma\gamma$" denotes the Higgs boson decaying into two photons.
- **Generators Used**: Specifies the simulation software used to generate the data. Information about the generators can be found in the [Simulation Tools section](../monte_carlo/simulation_tools)
- **Keywords**: These are specific terms or phrases associated with the dataset that help to find specific datasets. Some examples of keywords are: "SM", "BSM", "Higgs".
- **Description**: Provides additional information about the dataset. It may include details about the experimental setup, conditions under which the data was simulated, or any special characteristics of the dataset.
- **Job Option**: This includes a link to the specific code or configuration files used to generate the sample.

Use the search bar to find a specific dataset by its ID, keywords, or description. The search bar can locate any text within the table, allowing for flexible and comprehensive searches. You can also **download the csv version of the table <DownloadLink file="/files/metadata.csv">here</DownloadLink>**.

<FilterableTable />
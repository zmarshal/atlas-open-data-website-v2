# Simulation Tools
In ATLAS, a wide selection of simulation tools are utilized, each serving different purposes. Below is a list of some of the most common tools. Please note that this is not a comprehensive list, but rather a highlight of frequently used tools in various simulation parts of the [Monte Carlo production chain](../monte_carlo/MC_production_chain).

## Parton Distribution Functions (PDFs)
- [CTEQ/CT14](https://arxiv.org/pdf/1707.00657.pdf): A set of Parton Distribution Functions, CTEQ/CT14 provides detailed models of the momentum distribution of partons (quarks and gluons) inside protons and neutrons, crucial for accurate predictions in high-energy physics.
- [NNPDF](https://nnpdf.mi.infn.it): The NNPDF family offers a range of Parton Distribution Functions based on a methodology using neural networks. This approach aims to provide more accurate and unbiased representations of parton distributions.
- [MSTW/MRST](https://arxiv.org/pdf/0901.0002.pdf): The MSTW (formerly MRST) PDFs are another widely used set of parton distribution functions, offering critical insights into the structure of hadrons, essential for precision calculations in particle physics.

## Event Generation
- [Pythia](https://pythia.org/manuals/pdfdoc/pythia8300.pdf) and [Herwig](https://arxiv.org/pdf/0803.0883.pdf) : These are general purpose Monte Carlo event generators for describing collisions at high energies between electrons, protons, photons and heavy nuclei. They are able to produce leading-order calculations of physics processes alone, and is frequently used for showering, hadronization, and decay with higher-order matrix element generators.
- [Sherpa](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.15.html): Known for its ability to handle a broad spectrum of processes, Sherpa is an event generator that integrates several aspects. It is able to produce leading-order calculations of physics processes alone, and is frequently used for showering, hadronization, and decay with higher-order matrix element generators
- MadGraph: This tool is specialized in generating matrix elements for specific processes, particularly useful in scenarios involving new physics.
- MC@NLO: A program that implements the MC@NLO method, allowing for the matching of NLO (next-to-leading order) QCD calculations with event generators like Herwig and Pythia for more accurate predictions.
- [POWHEG](https://arxiv.org/pdf/1007.3893.pdf): POWHEG is a tool for implementing NLO calculations in parton shower Monte Carlo programs, known for its accuracy in handling higher-order corrections, crucial for precise theoretical predictions.
- [EvtGen](https://evtgen.hepforge.org/doc/EvtGenGuide.pdf): a specialized program for the decays of heavy hadrons (those containing c-quarks and b-quarks), used on top of Pythia and Herwig to improve their modeling of these hadrons.

## Detector Simulations
- [Geant4](https://geant4.web.cern.ch): Geant4 is extensively used in ATLAS for modeling the interactions of particles with the detector material, aiding in the design and optimization of the detector.
- [AtlFast3](https://arxiv.org/pdf/2109.02551.pdf): AtlFast3 is designed for rapid simulation of particle interactions with the detector, providing quicker but less detailed results compared to full Geant4 simulations.

## Software Framework
- Athena: Athena is the primary software framework used in ATLAS. It integrates a wide array of tools and software packages specifically designed for ATLAS. It plays an important role in data reconstruction and analysis due to its ability to efficiently process large volumes of data, translating complex particle interactions into analyzable information. Athena includes almost all the tools used for digitization and reconstruction processes, making it essential to the overall success of data interpretation in ATLAS experiments.
# Physics Searches: Standard Model
These notebooks dive into the world of Standard Model searches, exploring the fundamental particles and forces that constitute the universe as described by the Standard Model of particle physics. Through these analyses, we aim to test the predictions of the Standard Model, enhancing our understanding of the universe.

## Jupyter Notebooks
### Uproot
#### [Higgs to ZZ](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/HZZAnalysis.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to rediscover the Higgs boson yourself! You will discover the Higgs boson decaying into a pair of Z bosons, which are in turn decaying into a lepton-antilepton pair each.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FHZZAnalysis.ipynb)

#### [Higgs to ZZ with Boosted Decision Tree](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/HZZ_BDT_demo.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to apply a Machine Learning approach to discover the Higgs boson yourself! You will discover the Higgs boson decaying into a pair of Z bosons, which are in turn decaying into a lepton-antilepton pair each, and you will learn how to use a boosted decision tree (BDT) like a professional data analist in Physics!<br/>
Physics: ⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/58190f0a5f15d35ade86708965403a0ed7b5a87e?urlpath=lab%2Ftree%2F13-TeV-examples%2Fuproot_python%2FHZZ_BDT_demo.ipynb)

#### [Higgs to ZZ with a neural network](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/HZZ_NeuralNet_demo.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to apply a Machine Learning approach to discover the Higgs boson yourself! You will discover the Higgs boson decaying into a pair of Z bosons, which are in turn decaying into a lepton-antilepton pair each, and you will learn how to use a simple neural network like a professional data analist in Physics!<br/>
Physics: ⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FHZZ_NeuralNet_demo.ipynb)


#### [Higgs to ZZ with the Coffea framework](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/CoffeaHZZAnalysis.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to rediscover the Higgs boson yourself, with the Coffea framework!<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FCoffeaHZZAnalysis.ipynb)

#### [Higgs to γγ analysis](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/HyyAnalysis.ipynb) 
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to rediscover the Higgs boson yourself! You will discover the Higgs boson decaying into two photons.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FHyyAnalysis.ipynb)

#### [Find the Z boson yourself!](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/Find_the_Z.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to find the shape of the resonance of the Z boson.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FFind_the_Z.ipynb)

#### [Machine Learning for ttZ opposite-sign dilepton analysis](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/learning_rate_demo.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to implement Machine Learning in the opposite-sign dilepton analysis, with a special focus on the definition of the learning rate, following the ATLAS published paper [Measurement of the ttZ and ttW cross sections in proton-proton collisions at sqrt(s)= 13 TeV with the ATLAS detector"](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.99.072009).<br/>
Physics: ⭐⭐⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2Flearning_rate_demo.ipynb)

#### [Machine Learning for ttZ in ATLAS](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/ttZ_ML_from_csv.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to implement Machine Learning in the opposite-sign dilepton analysis, following the ATLAS published paper [Measurement of the ttZ and ttW cross sections in proton-proton collisions at sqrt(s)= 13 TeV with the ATLAS detector"](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.99.072009).<br/>
Physics: ⭐⭐⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FttZ_ML_from_csv.ipynb)

#### [Machine Learning and BDT for ttZ in ATLAS](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/ttZ_ML_from_root.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to implement Machine Learning, in particular a boosted decision tree, in the opposite-sign dilepton analysis, following the ATLAS published paper [Measurement of the ttZ and ttW cross sections in proton-proton collisions at sqrt(s)= 13 TeV with the ATLAS detector"](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.99.072009).<br/>
Physics: ⭐⭐⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FttZ_ML_from_root.ipynb)

### C++
#### [An introductional notebook to HEP analysis in C++](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/cpp/ATLAS_OpenData_13-TeV_simple_cpp_example_histogram.ipynb)
This notebook helps you to find an easy set of commands that show some basic computing techniques commonly used in High Energy Physics (HEP) analyzes, in few lines of code. It also shows how to create an histogram, fill it and draw it. Moreover it is an introduction to [ROOT](https://root.cern.ch/) too. The final output is a plot with the number of leptons. The library used is [ROOT](https://root.cern.ch/), a scientific data analysis software framework that provides a large set of functionalities needed to deal with big data processing, statistical analysis, visualisation and storage.<br/>
Physics: ⭐<br/>
Coding: ⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fcpp%2FATLAS_OpenData_13-TeV_simple_cpp_example_histogram.ipynb)

#### [Searching for the Higgs boson in the H→γγ channel](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/cpp/ATLAS_OpenData_13-TeV__analysis_example-cpp_Hyy_channel.ipynb) 
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to rediscover the Higgs boson decaying in two photons.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fcpp%2FATLAS_OpenData_13-TeV__analysis_example-cpp_Hyy_channel.ipynb)

### Python
#### [Get running the full H→γγ analysis using the 13 TeV dataset in 5 minutes!](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_python_full_HyyAnalysis_5min.ipynb) 
The analysis is based on the 13 TeV Open Data by the ATLAS Experiment. The ATLAS note [ATL-OREACH-PUB-2020-001](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-OREACH-PUB-2020-001) can be used as a guide on the content, properties, capabilities and limitations of the released datasets. In this tutorial, in about 5 minutes you are going to re-produce the H→γγ analysis plots from the note.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fpython%2FATLAS_OpenData_13-TeV_python_full_HyyAnalysis_5min.ipynb)

#### [Invariant mass recontruction using TLorentz_vectors](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_python_invariant_mass_reconstruction_using_TLorentz_vectors.ipynb) 
The analysis presented in this notebook consists in searching for events where one or two [Z bosons](https://en.wikipedia.org/wiki/W_and_Z_bosons) decay to two (or four) leptons of same flavour and opposite charge.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fpython%2FATLAS_OpenData_13-TeV_python_invariant_mass_reconstruction_using_TLorentz_vectors.ipynb)

#### [Simple two-samples comparison](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_python_simple_two_samples_comparison.ipynb)
The analysis presented in this notebook consists in comparing the kinematics between events coming from the Standard Model Higgs boson decaying to two W bosons to those coming from the Standard Model WW-diboson background production.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fpython%2FATLAS_OpenData_13-TeV_python_simple_two_samples_comparison.ipynb)

#### [A simple introductional notebook to HEP analysis in python](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_simple_python_example_histogram.ipynb)
In this notebook you can find an easy set of commands that show the basic computing techniques commonly used in high energy physics (HEP) analyzes. It also shows how to create a histogram, fill it and draw it. Moreover it is an introduction to [ROOT](https://root.cern.ch/) too. At the end you get a plot with the number of leptons.<br/>
Physics: ⭐<br/>
Coding: ⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fpython%2FATLAS_OpenData_13-TeV_simple_python_example_histogram.ipynb)

#### [Searching for the Higgs boson in the H→γγ channel](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/python/ATLAS_OpenData__13-TeV_analysis_example-python_Hyy_channel.ipynb)
In this notebook you can learn how to discover the Higgs boson at the LHC, using [ATLAS Open Data](http://opendata.atlas.cern).<br/>
Physics: ⭐<br/>
Coding: ⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fpython%2FATLAS_OpenData__13-TeV_analysis_example-python_Hyy_channel.ipynb)

<!-- ### RDataFrame
#### [Searching for the Higgs boson in the H→γγ channel in RDataFrame](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/rdataframe/RDataFrame_Hyy.ipynb)
In this notebook you can learn how to discover the Higgs boson at the LHC in the two-photon channel, using [ATLAS Open Data](http://opendata.atlas.cern) and ROOTDataFrame.<br/>
Physics: ⭐<br/>
Coding: ⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Frdataframe%2FRDataFrame_Hyy.ipynb)

#### [A simple example for the Higgs boson decay in the H→γγ channel in RDataFrame](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/rdataframe/RDataFrame_Hyy_SimpleExample.ipynb)
In this notebook you can inspect a simple example to discover the Higgs boson at the LHC in the two-photon channel, using [ATLAS Open Data](http://opendata.atlas.cern) and ROOTDataFrame.<br/>
Physics: ⭐<br/>
Coding: ⭐<br/>
Time: ⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Frdataframe%2FRDataFrame_Hyy_SimpleExample.ipynb) -->


## Kaggle Notebooks
### Python
#### [Introduction to Machine Learning completed](https://www.kaggle.com/code/meirinevans/introduction-to-machine-learning-completed/notebook)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to apply a Machine Learning approach to discover the Higgs boson yourself! You will discover the Higgs boson decaying into a pair of Z bosons, which are in turn decaying into a lepton-antilepton pair each.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐<br/>

#### [Matplotlib Data vs MC](https://www.kaggle.com/code/meirinevans/matplotlib-data-vs-mc)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to apply a Machine Learning approach to discover the Higgs boson yourself! In particular, you will learn how to use the Matplotlib tool to compare real data from the ATLAS Experiment at the LHC with simulated Monte Carlo data, which are generated to mimic the real Physics of the Higgs boson.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>

#### [How to rediscover the Higgs!](https://www.kaggle.com/code/ellabanham/how-to-rediscover-the-higgs)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to apply a Machine Learning approach to discover the Higgs boson yourself!<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>

#### [How to rediscover the Higgs!](https://www.kaggle.com/code/meirinevans/how-to-rediscover-the-higgs)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to apply a Machine Learning approach to discover the Higgs boson yourself!<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐<br/>

#### [W branching ratio](https://www.kaggle.com/code/meirinevans/w-branching-ratio)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to measure the W-boson decay branching ratios yourself!<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐<br/>

# Online Platforms
Using online platforms is the quickest and easiest way to start analyzing ATLAS open data. The following platforms are aimed to run **analysis in notebooks**.

## Binder
Binder simplifies the process of running Jupyter notebooks by creating custom, shareable computing environments. Here’s how you can use Binder:

1. **Choose a Notebook:** Navigate to either the [Physics Searches: Standard Model](../13tutorial) or [Physics Searches: Beyond Standard Model and Exotics](../13tutorial-BSM) sections to select an analysis.
2. **Launch Binder:** Click on the ![Binder](https://mybinder.org/badge_logo.svg) badge below the description of the notebook that you wish to run.
3. **Wait** for it to load.
4. **Analyze the Data**: Start your analysis in the Binder environment.

For more complex needs, such as running multiple notebooks review the [Binder Documentation](https://mybinder.readthedocs.io/en/latest/#) for guidance on setting up a more personalized Binder environment.

## SWAN
[SWAN](https://swan.web.cern.ch/swan/) (Service for Web-based ANalysis) is exclusive to CERN users, offering a robust platform for interactive data analysis directly in the cloud.

1. **Select a Notebook:** As a CERN user, choose your desired notebook from the [Physics Searches: Standard Model](../13tutorial) or [Physics Searches: Beyond Standard Model and Exotics](../13tutorial-BSM) sections. This will direct you to the GitHub repository for download.
To download the notebook, click on "donwload raw file", in the top right corner of the file

import githubdownload from '../pictures/download_github.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={githubdownload} alt="donwload raw file button in github" style={{width: 300, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: "Donwload raw file" button in GitHub.</figcaption>
</figure>


2. **Access SWAN:** Visit [SWAN](https://swan.cern.ch/) and start a session.
3. **Upload the notebook**.
4. **Analyze the Data**: Start your analysis in the SWAN environment.

For detailed instructions and additional features check out the [SWAN documentation](https://swan.docs.cern.ch/intro/what_is/).

## Kaggle

[Kaggle](https://www.kaggle.com) is a popular platform for data science competitions and also hosts a variety of notebooks for analysis, including those from ATLAS open data.

To use Kaggle for ATLAS open data:

1.  **Access the Analysis**: Click on the link to the notebook within the [Physics Searches: Standard Model](../13tutorial) or [Physics Searches: Beyond Standard Model and Exotics](../13tutorial-BSM) sections.
2.  **Edit the Notebook**: In the top right cornet, select "Copy and Edit" to begin interacting with the pre-configured notebook.

import imagekaggle from '../pictures/kaggle_button.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={imagekaggle} alt="'Copy and Edit' button in kaggle" style={{width: 300, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 2: "Copy and Edit" button in kaggle.</figcaption>
</figure>

3.  **Analyze the Data**: Start your analysis directly in the Kaggle environment.

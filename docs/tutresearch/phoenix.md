# Phoenix for Event Visualisation with PHYSLITE
> 🚧 May 2024: Under construction

Visualizing an event inside the detector opens a path to a deeper understanding of the physical processes resulting from a collision. One commonly used tool for event visualisation is [Phoenix](https://phoenixatlas.web.cern.ch/PhoenixATLAS/), a framework that allows 3D visualisation of collision events inside the detector.

<div class="responsive-iframe-container" style={{ borderRadius:'15px', minHeight: '50vh' }}>
    <iframe src="https://phoenixatlas.web.cern.ch/PhoenixATLAS/?file=./docs/tutresearch/testing123.json&theme=dark&type=json" frameborder="0" allowfullscreen></iframe>
</div>

The event in the image above is ___. However, you can display any event of your choice by utilizing the open data available in PHYSLITE format. In the following subsection, you will find a guide through the process of utilizing Athena software to achieve this visualization.

## Transforming PHYSLITE to JSON
Phoenix accepts data in JSON or JiveXML formats. We will focus in the [JSON format](https://github.com/HSF/phoenix/blob/main/guides/developers/event_data_format.md).

To transform a PHYSLITE file into a JSON for Phoenix, you need:
1. **PHYSLITE file**: The datasets can be found in the [CERN Open Data portal](https://opendata.cern.ch), and the instructions on how to search for one of our open data PHYSLITE datasets are in [Accessing the Data](../documentation/data_format/access). 
2. **Athena software**: The [Athena software](../documentation/monte_carlo/simulation_tools) can be run locally in a container, instructions for this are in [Getting Started with Containers for Analysis](containers).

To transform the PHYSLITE into a JSON file that can be read by Phoenix, we are going to use the [`DumpEventDataToJSON`](https://gitlab.cern.ch/atlas/athena/-/tree/main/Event/DumpEventDataToJSON) algorithm, included in `Athena`. First, set up `Athena`>=*25.0.2*.

```
Under construction 🚧
```
To use the `DumpEventDataToJSON` algorithm, use the following command:
```
python -m DumpEventDataToJSON.DumpEventDataToJSONConfig --filesInput path/to/physlite -o name_output.json
```
At this point you should have a JSON, `name_of_file.json`, to upload to Phoenix. You can open your own files using the file upload dialog (third, right to left, in the figure below)

import uploadPhoenix from './images/phoenix_bar.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={uploadPhoenix} alt="You can open your own files using the File upload dialog (third, right to left) " style={{width: 1000, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: Phoenix bar. The file upload dialog is the one inside the red square.</figcaption>
</figure>

For more information about the use of Phoenix, see the [Phoenix User Guide](https://github.com/HSF/phoenix/blob/main/guides/users.md).
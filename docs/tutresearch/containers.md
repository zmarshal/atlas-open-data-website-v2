# Getting Started with Containers for Analysis
Welcome to our tutorial on setting up containers for analysis! This guide will help you get started quickly and easily.

For running analysis you can refer to the ATLAS Analysis Software Tutorial. To be able to follow it you will need to setup your enviroment. For people outside of the ATLAS collaboration we offer docker containers. This can be run locally and require minimal set up. For those outside the ATLAS Collaboration, we offer Docker containers that you can run locally with minimal setup. Detailed instructions can be found on our [Twiki page](https://twiki.atlas-canada.ca/bin/view/AtlasCanada/Containers#Standalone_Script), which also includes other setup options if you want to explore further.

:::note For ATLAS Collaboration Members
If you are a member of the ATLAS Collaboration, you don't need to set up these containers. Instead, you can use the tools available on lxplus and follow the [ATLAS Analysis Software Tutorial](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/) directly.
:::

## Setting Up Your Environment
Before starting, ensure you have Docker installed on your system. If not, please follow the installation instructions on the [official Docker website](https://docs.docker.com/engine/install/).

To set up your environment for analysis, we'll install a minimal version of ATLASLocalRootBase. Follow these steps:

1. **Download the Setup Script**:
   - Visit [setupATLAS](https://atlas-tier3-sw.web.cern.ch/containers/setupATLAS) to find the necessary script.
   - Alternatively, you can download it directly via the command line:
     ```sh
     curl -O https://atlas-tier3-sw.web.cern.ch/containers/setupATLAS
     ```
     or
     ```sh
     wget https://atlas-tier3-sw.web.cern.ch/containers/setupATLAS
     ```

2. **Make the Script Executable**:
   ```sh
   chmod +x ./setupATLAS
   ```

3. **Check Available Containers**:
   To see which containers are available, run:
   ```sh
   <path to>/setupATLAS -c --showVersions
   ```

4. **Get Help with the Script**:
   For help with the script, run:
   ```sh
   <path to>/setupATLAS -c -h
   ```

### Running a Container Example

Here is an example of how to run a container, specifically for AnalysisBase, version 25.2.2:
```sh
./setupATLAS -c gitlab-registry.cern.ch/atlas/athena/analysisbase:25.2.2
```

With these steps, you'll be ready to start your analysis in no time. Happy analyzing!
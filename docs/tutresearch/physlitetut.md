# Using the PHYSLITE Format
The research data is available in the [PHYSLITE format](../documentation/data_format/physlite.md), which is user-friendly and ready for analysis. This notebook demonstrates how to utilize ATLAS Open Data in PHYSLITE format using `uproot` and `awkward` arrays for a basic physics analysis. Specifically, it shows how to reconstruct the hadronically decaying top quark from semi-leptonic $t\bar{t}$ events.

:::notebook [PHYSLITE Tutorial](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/physlite_tutorial.ipynb)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=for-research%2Fphyslite_tutorial.ipynb)
:::

## What's Inside the Notebook
In this notebook, you will learn:
- How to read PHYSLITE data with `uproot` and inspect its branches.
- How to compile branches into records.
- How to perform basic event and object selection.
- How to conduct basic overlap removal.

These steps will guide you to the top quark reconstruction.

## Transforming PHYSLITE to NTuple
To convert PHYSLITE to an NTuple, follow these sections from the [Analysis Software Tutorial](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/):

1. **Basic Analysis Algorithm**: From [Introduction to Algorithms](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/alg_basic_intro/) to [Run Algorithm](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/alg_basic_run/).
2. **CP Algorithms**: All subsections starting with [Common CP Algorithms Introduction](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/cpalg_intro/).
3. **Physics Objects**: [Electrons in Analysis](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/obj_el_analysis/), [Muons in Analysis](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/obj_mu_analysis/), [Jets in Analysis](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/obj_jet_analysis/).

For these you will need to use AnalysisBase. Check how to setup a container in the [next section](containers).
---
id: updates
title: "What changed?"
---

# The new ATLAS Open Data website
Changing is always tough, but sometimes necessary. We noticed that we had too many resources scattered around, and too little information about where is what.
Below you will find a detailed breakdown of all the resources we have, where they were, and where they are now.

Feedback and suggestions are much appreciated, so if something is missing or could be done better, please [get in touch](/contact-us)!

---

## Known issues

### Lost 8 TeV samples
Please follow the updates at [MYATLAS-113](https://its.cern.ch/jira/projects/MYATLAS/issues/MYATLAS-113).

### Jupyter Notebooks have some deprecated code
Please follow the updates at [MYATLAS-114](https://its.cern.ch/jira/projects/MYATLAS/issues/MYATLAS-114).

---

## Structural changes, backups and old links

### 10/05/2024
#### Introduction of Public Likelihoods documentation and 13 TeV docs update.

- A new tool for exploring statistical workspaces is now present at the [public likelihoods](tutresearch/public_likelihoods) page.
- Update to the 13 TeV documentation:
  - General reorganisation of the content.
  - Introduction of the live dataset navigator (to be upgraded).
  - Fix broken links and minor revisions.

See merge requests [!12](https://gitlab.cern.ch/atlas-outreach-data-tools/atlas-open-data-website-v2/-/merge_requests/12) and [!13](https://gitlab.cern.ch/atlas-outreach-data-tools/atlas-open-data-website-v2/-/merge_requests/13).

### 26/04/2024
#### Major spring cleaning and upgrades.

- Upgrade of the Docusaurus framework to v3: this fixes a number of bugs, e.g. table of content sidebars stuck in the middle of the page.
- General re-organisation of content and website structure aimed to:
  - Improve the clarity of the resources that we provide.
  - Improve the website's navigability through category cards.
  - Fix broken links and redirection of old resources to the new domains.
- Introduction of the Open Data for research release.

See merge request [!10](https://gitlab.cern.ch/atlas-outreach-data-tools/atlas-open-data-website-v2/-/merge_requests/10).

### 29/11/2023
#### 8 TeV documentation

We keep a backup of the documentation at https://atlas-opendata.web.cern.ch/release/2020/documentation/8TeVDoc/

You can find all these resources in the new website:
- Dataset description: [8 TeV Open Datasets](documentation/overview_data/data_education_2016)
- Tutorials, Tools and infrastructure: [8 TeV Open Data Tutorial](category/8-tev-tutorials-for-education)

All the samples that once were at https://opendata.atlas.cern/release/samples/, now are on the backup website at https://atlas-opendata.web.cern.ch/release/2016/.

#### 13 TeV documentation

We keep a backup website at https://atlas-opendata.web.cern.ch/release/2020/documentation

You can find all these resources in the new website:
- Dataset description: [13 TeV Open Datasets](documentation/overview_data/data_education_2020)
- Tools and infrastructure: [How to use the data](category/13-tev-tutorials-for-education)